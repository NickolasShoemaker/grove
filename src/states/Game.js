//require other components

export default class Game extends Phaser.State{
	chance_it(){
		return Math.random() * 100;
	}

	weighted_gen(){
		/*let rng = new Phaser.RandomDataGenerator(378);
		let chance = rng.integerInRange(1, 100);*/
		let chance = this.chance_it();

		if(chance < 50){
			return 5;
		}else if(chance < 60){
			return 6;
		}else if(chance < 85){
			return 4;
		}else if(chance < 90){
			return 3;
		}else if(chance < 95){
			return 2;
		}else if(chance < 100){
			return 1;
		}
	}

	/*
	* Note, the index will change if the map size changes
	* hash values from 0 - mapHeight, 0 - mapWidth
	*/
	coordsToIndex(x, y){
		return x + (y * this.mapWidth);
	}

	onTap(){
		let tile = this.findTileInArray();
		
		if(tile.x >= 0 && tile.y >= 0){
			let index = this.coordsToIndex(tile.y, tile.x);
			if(index >= 0){
				let space;

				if(this.selected.length > 0){
					let sel = this.selected.pop();

					space = this.hexGrid.getChildAt(sel);
					space.tint = 0xFFFFFF;
				}

				space = this.hexGrid.getChildAt(index);
				space.tint = 0x40db25;

				this.selected.push(index);
			}
		}
	}

	/*
	* Find tile coord from array coords
	*/
	findTileInSpace(pos){
		let new_x, new_y;

		if(pos.y % 2 == 0){
			new_x = (this.hexTileWidth / 2) + (this.hexTileWidth * pos.x);
		}else{
			new_x = this.hexTileWidth + (this.hexTileWidth * pos.x);
		}

		new_y = (this.hexTileHeight / 2) + (pos.y * (this.hexTileHeight * 3/4));

		pos.x = new_x;
		pos.y = new_y;

		return pos;
	}

	/*
	* Find array coords from click
	*/
	findTileInArray(){
	    let pos = this.input.getLocalPosition(this.hexGrid, this.input.activePointer);
	    if(pos.x < 0 || pos.x > this.hexGrid.width || pos.y < 0 || pos.y > this.hexGrid.height)
	    	return -1;

	    let tileHeight = 40;

	    let clickBoxX = (pos.x - (pos.x % this.hexTileWidth)) / this.hexTileWidth;
	    let clickBoxY = (pos.y - (pos.y % (tileHeight))) / (tileHeight);//this.hexTileHeight * 2

	    let Xindex = clickBoxX * 2;
	    let Yindex = clickBoxY;

	    let Xoff = (pos.x % this.hexTileWidth);
	    let Yoff = (pos.y % tileHeight);
	    let Y2off = (pos.y % (tileHeight / 2));
	    
	    let inY = ((pos.y - (pos.y % (tileHeight / 2))) / (tileHeight / 2)) % 2;
	    let inX;
	    if(Xoff < 22){
	    	inX = 0;
	    }else if(Xoff < 42){
			inX = 1;
	    }else if(Xoff < 64){
			inX = 2;
	    }else{
			inX = 3;
	    }

		let slope = this.hexTileHeight / (44);
	    let invSlope = -1 / slope;

	    let Yintercept = Xoff * slope;
	    let YinVintercept = Xoff * invSlope;

	    console.log("clickBox: (" + clickBoxX + "," + clickBoxY + ")");
	    console.log("subBox: (" + inX + "," + inY + ")");
	    console.log("index: (" + Xindex + "," + Yindex + ")");
	    console.log("Y: " + Yoff + ", Yintercept: " + Yintercept + ", YinVintercept: " + YinVintercept);

	    if(inY == 0){
			if(inX == 0){
				if(Yoff < Yintercept){
					Yindex--;
					Xindex--;
				}else{
					//same
				}
		    }else if(inX == 1){
				//same
		    }else if(inX == 2){
				if(Yoff < YinVintercept){
					Yindex--;
			    	Xindex++;
				}else{
					//same
				}
		    }else{
				Yindex--;
		    	Xindex++;
		    }
	    }else{
	    	if(inX == 0){
		    	if(Yoff < YinVintercept){
		    		//same
		    	}else{
		    		Xindex--;
		    	}
		    }else if(inX == 1){
				//same
		    }else if(inX == 2){
				if(Yoff < Yintercept){
					//same
				}else{
					Xindex++;
				}
		    }else{
		    	Xindex++;
		    }
	    }
	   
		console.log("final index: (" + Xindex + "," + Yindex + ")");

		if(Yindex >= this.mapHeight || Xindex >= this.mapWidth){
			console.log("Warn index: (" + Xindex + "," + Yindex + "), out of range.");
			return -1;
		}

	   pos.y = Yindex;
	   pos.x = Xindex;
	   return pos;
	}

	constructor(){
		//object level properties
		super();

		this.hexTileHeight = 26;	//The verticle distance between stacked tiles
		this.hexTileWidth = 85;		//The horizontal between tiles
		this.mapHeight = 14;
		this.mapWidth = 14;

		/*
		* Create and populate map
		*/
		this.levelData = [];
		for(let i = 0; i < this.mapHeight; i++){
			this.levelData[i] = [];
			for(let j = 0; j < this.mapWidth; j++){
				this.levelData[i][j] = this.weighted_gen();
			}
		}

		/*
		* Indicies of selected tiles
		*/
		this.selected = [];

		/*
		* Create a tile dictionary, now it only holds sprites but we can also store other data
		*/
		this.dictionary = [
			['base', null, false],

			['tiles', 'FoodTile_0.png', false],
			['tiles', 'FoodTile_1.png', false],
			['tiles', 'FoodTile_2.png', false],

			['tiles', 'Grove Trees.png', false],
			['tiles', 'Tile grass.png', false],
			['tiles', 'Stone Tile.png', false],

			['tiles', 'Stone cutter.png', false],
			['tiles', 'Tree House.png', false],

			['tiles', 'WaterTile_0.png', false],
			['tiles', 'WaterTile_1.png', false],
			['tiles', 'WaterTile_2.png', false],
			['tiles', 'WaterTile_3.png', false],
			['tiles', 'WaterTile_4.png', false],
			['tiles', 'WaterTile_5.png', false],
			['tiles', 'WaterTile_6.png', false],
			['tiles', 'WaterTile_7.png', false],

			['selector', null, false],
		];
	}


	create(){
		this.hexGrid = this.add.group();

		let verticalOffset = 39; //this.hexTileHeight * 3/4
		let horizontalOffset = this.hexTileWidth;
		let startXInit = this.hexTileWidth / 2;
		let startYInit = this.hexTileHeight / 2;
		let startX;
		let startY;

		let hexTile;
		for(let x = 0; x < this.levelData[0].length; x++){
		    if(x % 2 != 0){
		        startX = startXInit;//2 *
		    }else{
		        startX = startXInit;
		    }

			startX += (x * 43);

		    for(let y = 0; y < this.levelData.length; y++){
	    		if(x % 2 != 0){
					startY = (startYInit * 2) + 6;
	    		}else{
	    			startY = startYInit;
	    		}
			    startY += (y * verticalOffset);

	            hexTile = new HexTile(this, startX, startY, 
	            	this.dictionary[this.levelData[y][x]][0], 
	            	this.dictionary[this.levelData[y][x]][1], 
	            	this.dictionary[this.levelData[y][x]][2], x, y);

	            this.hexGrid.addChildAt(hexTile, this.coordsToIndex(y, x));
	            //this.add.text(startX, startY, "(" + x + "," + y + ")");
		    }
		}		

		/*for(let i = 0; i < this.levelData.length; i++){
		    if(i % 2 != 0){
		        startX = 2 * startXInit;
		    }else{
		        startX = startXInit;
		    }

		    startY = startYInit + (i * verticalOffset);
		    for(let j = 0; j < this.levelData[0].length; j++){
	            hexTile = new HexTile(this, startX, startY, this.dictionary[this.levelData[i][j]][0], this.dictionary[this.levelData[i][j]][1], this.dictionary[this.levelData[i][j]][2], i, j);
	            this.hexGrid.addChildAt(hexTile, this.coordsToIndex(j, i));
	            this.add.text(startX, startY, "(" + j + "," + i + ")");
		        
		        startX += horizontalOffset;
		    }
		}*/

		this.hexGrid.x = 10;
		this.hexGrid.y = 10;

		this.input.onTap.add(this.onTap, this);
	}

	update(){
		if(this.input.keyboard.isDown(Phaser.Keyboard.LEFT)){
			this.hexGrid.x -= 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.RIGHT)){
			this.hexGrid.x += 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.UP)){
			this.hexGrid.y += 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.DOWN)){
			this.hexGrid.y -= 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.W)){
			this.hexGrid.width += 10;
			this.hexGrid.height += 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.S)){
			if(this.hexGrid.width > 0 || this.hexGrid.height > 0)
				this.hexGrid.width -= 10;
				this.hexGrid.height -= 10;
		}else if(this.input.keyboard.isDown(Phaser.Keyboard.C)){
			this.hexGrid.width = 800;
			this.hexGrid.height = 600;
			this.hexGrid.x = 10;
			this.hexGrid.y = 10;
		}
	}
}