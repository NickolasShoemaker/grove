export default class Preload{

  constructor(){
    this.asset = null;
    this.ready = false;
  }

  /*
  * We add the loading asset to the cache so we can give
  * the user visual feedback as we get set up
  */
  preload(){
    this.load.image('loading_bg', 'assets/images/loading_bg.jpg');
  }

  /*
  * Load the game assets into the game cache
  */
  create(){
    //background for game
    this.add.sprite(0,0, "loading_bg");

    this.asset = this.add.sprite(this.game.width/2,this.game.height/2, 'preloader');
    this.asset.anchor.setTo(0.5, 0.5);

    // Add a one time event to start the game when everything is loaded
    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
    this.load.setPreloadSprite(this.asset);

    //do all your loading here
    this.load.image('base', 'assets/images/Base Tile.png');
    this.load.image('selector', 'assets/images/Selector.png');
    /*this.load.image('grass', 'assets/images/Tile grass.png');
    this.load.image('trees', 'assets/images/Grove Trees.png');*/
    this.load.atlasJSONHash('tiles', 'assets/images/tileatlas.png', 'assets/images/tileatlas.json');

    //staaaart load
    this.load.start();
  }

  /*
  * Check for on load in a busy loop
  */
  update(){
    if(this.ready){
      this.game.state.start('game');
    }
  }

  onLoadComplete(){
    this.ready = true;
  }
}